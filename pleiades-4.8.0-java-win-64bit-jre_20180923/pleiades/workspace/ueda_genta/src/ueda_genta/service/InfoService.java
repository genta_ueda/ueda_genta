package ueda_genta.service;

import static ueda_genta.utils.CloseableUtil.*;
import static ueda_genta.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServlet;

import ueda_genta.beans.UserInfo;
import ueda_genta.dao.InfoDao;

public class InfoService extends HttpServlet {
	public List<UserInfo> getInfo() {

        Connection connection = null;
        try {
            connection = getConnection();

            InfoDao infoDao = new InfoDao();
            List<UserInfo> ret = infoDao.select(connection);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public UserInfo getEdit(int value) {

        Connection connection = null;
        try {
            connection = getConnection();

            InfoDao InfoDao = new InfoDao();
            UserInfo user = InfoDao.getEdit(connection, value);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


}


