package ueda_genta.service;

import static ueda_genta.utils.CloseableUtil.*;
import static ueda_genta.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServlet;

import ueda_genta.beans.Department_position;
import ueda_genta.dao.Department_positionDao;

public class Department_positionService extends HttpServlet {

    public List<Department_position> getDepartment_position() {

        Connection connection = null;
        try {
            connection = getConnection();

            Department_positionDao department_positionDao = new Department_positionDao();
            List<Department_position> ret = department_positionDao.select(connection);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


}
