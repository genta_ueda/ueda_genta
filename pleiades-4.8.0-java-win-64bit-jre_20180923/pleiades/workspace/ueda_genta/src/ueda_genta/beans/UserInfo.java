package ueda_genta.beans;

import java.io.Serializable;
import java.util.Date;

public class UserInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String account;
    private String password;
    private String name;
    private int branch;
    private String branch_name;
    private int department_position;
    private String department_position_name;
    private Date createdDate;
    private Date updatedDate;


    public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBranch() {
		return branch;
	}
	public void setBranch(int branch) {
		this.branch = branch;
	}
	public String getBranch_name() {
		return branch_name;
	}
	public void setBranch_name(String branch_name) {
		this.branch_name = branch_name;
	}
	public int getDepartment_position() {
		return department_position;
	}
	public void setDepartment_position(int department_position) {
		this.department_position = department_position;
	}
	public String getDepartment_position_name() {
		return department_position_name;
	}
	public void setDepartment_position_name(String department_position_name) {
		this.department_position_name = department_position_name;
	}

	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}


}
