package ueda_genta.beans;

import java.io.Serializable;
import java.util.Date;

public class Department_position implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String department_position_name;
    private Date createdDate;
    private Date updatedDate;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDepartment_position_name() {
		return department_position_name;
	}
	public void setDepartment_position_name(String department_position_name) {
		this.department_position_name = department_position_name;
	}

	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}


}