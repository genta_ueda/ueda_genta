package ueda_genta.dao;

import static ueda_genta.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ueda_genta.beans.UserInfo;
import ueda_genta.exception.SQLRuntimeException;

public class InfoDao {

    public List<UserInfo> select(Connection connection) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id as id,");
            sql.append("users.account as account,");
            sql.append("users.password as password,");
            sql.append("users.name as name,");
            sql.append("branchs.branch_name as branch_name,");
            sql.append("department_positions.department_position_name as department_position_name ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branchs ");
            sql.append("ON users.branch = branchs.id ");
            sql.append("INNER JOIN department_positions ");
            sql.append("ON users.department_position = department_positions.id ");



            ps = connection.prepareStatement(sql.toString());


            ResultSet rs = ps.executeQuery();
            List<UserInfo> ret = toUserInfoList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserInfo> toUserInfoList(ResultSet rs)
            throws SQLException {

        List<UserInfo> ret = new ArrayList<UserInfo>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                String account = rs.getString("account");
                String password = rs.getString("password");
                String name = rs.getString("name");
                String branch_name = rs.getString("branch_name");
                String department_position_name = rs.getString("department_position_name");


                UserInfo userinfo = new UserInfo();
                userinfo.setId(id);
                userinfo.setAccount(account);
                userinfo.setPassword(password);
                userinfo.setName(name);
                userinfo.setBranch_name(branch_name);
                userinfo.setDepartment_position_name(department_position_name);
                ret.add(userinfo);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public UserInfo getEdit(Connection connection, int id) {

	    PreparedStatement ps = null;
	    try {
	    	 StringBuilder sql = new StringBuilder();
	            sql.append("SELECT ");
	            sql.append("users.id as id, ");
	            sql.append("users.branch as branch, ");
	            sql.append("users.department_position as department_position, ");
	            sql.append("users.account as account, ");
	            sql.append("users.password as password, ");
	            sql.append("users.name as name, ");
	            sql.append("branchs.branch_name as branch_name, ");
	            sql.append("department_positions.department_position_name as department_position_name ");
	            sql.append("FROM users ");
	            sql.append("INNER JOIN branchs ");
	            sql.append("ON users.branch = branchs.id ");
	            sql.append("INNER JOIN department_positions ");
	            sql.append("ON users.department_position = department_positions.id ");
	            sql.append("WHERE users.id = ? ");

	        ps = connection.prepareStatement(sql.toString());
	        ps.setInt(1, id);

	        ResultSet rs = ps.executeQuery();
	        List<UserInfo> userList = toUserEditList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	private List<UserInfo> toUserEditList(ResultSet rs)
            throws SQLException {

        List<UserInfo> ret = new ArrayList<UserInfo>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                int branch = rs.getInt("branch");
                int departmentPosition = rs.getInt("department_position");
                String account = rs.getString("account");
                String password = rs.getString("password");
                String name = rs.getString("name");
                String branchName = rs.getString("branch_name");
                String department_positionName = rs.getString("department_position_name");

                UserInfo info = new UserInfo();
                info.setId(id);
                info.setBranch(branch);
                info.setDepartment_position(departmentPosition);
                info.setAccount(account);
                info.setPassword(password);
                info.setName(name);
                info.setBranch_name(branchName);
                info.setDepartment_position_name(department_positionName);

                ret.add(info);
            }
            return ret;
        } finally {
            close(rs);
        }

    }

}
