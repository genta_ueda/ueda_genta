package ueda_genta.dao;

import static ueda_genta.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ueda_genta.beans.Branch;
import ueda_genta.exception.SQLRuntimeException;

public class BranchDao {

	public List<Branch> select(Connection connection) {

        PreparedStatement ps = null;

        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("branchs.id as id, ");
            sql.append("branchs.branch_name as branch_name ");
            sql.append("FROM branchs ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Branch> ret = toBranchList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	private List<Branch> toBranchList(ResultSet rs)
            throws SQLException {

        List<Branch> ret = new ArrayList<Branch>();
        try {
            while (rs.next()) {
                String branchName = rs.getString("branch_name");
                int id = rs.getInt("id");


                Branch branch = new Branch();
                branch.setBranch_name(branchName);
                branch.setId(id);

                ret.add(branch);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}
