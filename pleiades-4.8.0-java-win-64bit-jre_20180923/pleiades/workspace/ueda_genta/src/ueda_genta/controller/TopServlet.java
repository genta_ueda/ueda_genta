package ueda_genta.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ueda_genta.beans.UserInfo;
import ueda_genta.service.InfoService;


@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	 List<UserInfo> info = new InfoService().getInfo();

    	 request.setAttribute("info", info);
        request.getRequestDispatcher("/top.jsp").forward(request, response);
    }

}